package model;

public class Imperatrice extends Piece implements I_Piece{

	public Imperatrice(int x, int y, boolean isBlack) {
		super(x, y, isBlack);
	}
	
	public String getType() {
		return "Imperatrice";
	}
	
	public boolean mouvementValide(int x, int y, Piece[] board) {
		int size=board.length/8;
		if(board[x-1+(y-1)*size]!=null)
		{
			if(board[x-1+(y-1)*size].getIsBlack()==this.isBlack)	return false;
		}
		if(this.y==y && this.x!=x)
		{
			int depX=this.x-x;
			if(depX>0)
			{
				for(int i=this.x-1;i>x;i--)
				{
					if(board[i-1+(y-1)*size]!=null)
						return false;
				}
				return true;
			}
			for(int i=this.x+1;i<x;i++)
			{
				if(board[i-1+(y-1)*size]!=null)
					return false;
			}
			return true;
		}
		
		if(this.x==x && this.y!=y)
		{
			int depY=this.y-y;
			if(depY<0)
			{
				for(int j=this.y+1;j<y;j++)
				{
					if(board[x-1+(j-1)*size]!=null)
					{
						return false;
					}
				}
				return true;
			}
			for(int j=this.y-1;j>y;j--)
			{
				if(board[x-1+(j-1)*size]!=null)
				{
					return false;
				}
			}
			return true;
		}
		if(((this.x+2==x || this.x-2==x) && (this.y+1==y || this.y-1==y)) || ((this.y+2==y || this.y-2==y) && (this.x+1==x || this.x-1==x)))
		{
			if(board[x-1+(y-1)*size]!=null)
			{
				if(board[x-1+(y-1)*size].getIsBlack()==this.isBlack)	return false;
			}
			return true;
		}
		return false;
	}
}



