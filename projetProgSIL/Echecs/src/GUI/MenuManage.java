package GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JRadioButtonMenuItem;
import javax.swing.event.MenuListener;

import control.Game;

public class MenuManage implements ActionListener{

	private Game game;
	public MenuManage(Game game) {
		this.game=game;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		JRadioButtonMenuItem tmp=(JRadioButtonMenuItem)e.getSource();
		String variante=tmp.getText();
		game.changeBoard((variante=="Capablanca")?2:1);
		
	}

}
