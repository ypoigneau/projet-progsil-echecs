package GUI;
import java.awt.event.*;
import model.Piece;
import control.Game;


/**
 * This object instanciate the behavior of the mouse on the guiBoard.
 *
 */
public class MouseManage extends MouseAdapter{

	private Game game;
	private Piece currentSelected;


	public MouseManage(Game game) {
		this.game=game;
	}

	public void mouseClicked(MouseEvent m) {

		int x,y;
		boolean currentPlayer = game.getCurrentPlayer();

		if(m.getClickCount()==1 && m.getButton()==MouseEvent.BUTTON1)
		{	 
			x=(m.getX()-5)/50;
			y=(m.getY()-5)/50;			
		
			Piece p=game.getPiece(x, y);			
			if(p!=null && currentSelected == null && (p.getIsBlack()==currentPlayer))
			{
				currentSelected=p;				
			}
			else if(currentSelected != null)
			{
				game.move(currentSelected,x,y);
				currentSelected=null;
			}
		}
	}

}



