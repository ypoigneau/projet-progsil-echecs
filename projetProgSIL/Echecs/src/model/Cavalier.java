package model;

/**
 * Implements the behavior of the piece of type Knight
 */
public class Cavalier extends Piece implements I_Piece{

	public Cavalier(int x, int y, boolean isBlack) {
		super(x, y, isBlack);
	}

	@Override
	public boolean mouvementValide(int x, int y, Piece[] board) {
		int size=board.length/8;
		if(((this.x+2==x || this.x-2==x) && (this.y+1==y || this.y-1==y)) || ((this.y+2==y || this.y-2==y) && (this.x+1==x || this.x-1==x)))
		{
			if(board[x-1+(y-1)*size]!=null)
			{
				if(board[x-1+(y-1)*size].getIsBlack()==this.isBlack)	return false;
			}
			return true;
		}
		return false;
	}

	public String getType() {
		return "Cavalier";
	}
}
