package GUI;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.*;
import java.awt.Color;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import control.Game;

/**
 	This object instanciate the main frame of the game 
 */




public class Game_GUI extends JFrame{

	//The guiBoard of the game
	private Damier_GUI guiBoard;	
	//The instance of the game
	private Game game;
	private JPanel infoJoueur;
	private JLabel j1;
	private JLabel j2;
	private JMenuBar menuBar;
	private JMenu menuVar;
	private JRadioButtonMenuItem classic;	
	private JRadioButtonMenuItem capablanca;
	private MouseManage mouse;


	/**
	 * Initialisation of the frame
	 */
	public Game_GUI(Game play,int variante){ 

		
		game=play;
		mouse=new MouseManage(play);
		menuBar=new JMenuBar();
		menuVar=new JMenu("Variante de jeu");
		classic=new JRadioButtonMenuItem("Classic");
		classic.addActionListener(new MenuManage(game));
		capablanca=new JRadioButtonMenuItem("Capablanca");
		capablanca.addActionListener(new MenuManage(game));
		menuVar.add(classic);
		menuVar.add(capablanca);
		menuBar.add(menuVar);
		setTitle("Jeu d'echec");		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBackground(Color.white);  		
		setResizable(false); 
		infoJoueur = new JPanel();
		infoJoueur.add(new JLabel("Temps restant: "),BorderLayout.NORTH);		
		j1=new JLabel("Joueur Noir : 30:00");		
		j2=new JLabel("Joueur Blanc: 30:00");
		j1.setBorder(BorderFactory.createLineBorder(Color.red));
		infoJoueur.add(j1,BorderLayout.EAST);
		infoJoueur.add(j2,BorderLayout.WEST);
		menuBar.setEnabled(true);
		
		if(variante==1)
		{
			setSize(500,550);     
			classic.setSelected(true);
			capablanca.setSelected(false);


		}
		else
		{
			setSize(600,550);   
			classic.setSelected(false);
			capablanca.setSelected(true);
		}
		getContentPane().add(menuBar,BorderLayout.NORTH);
		getContentPane().add(infoJoueur,BorderLayout.SOUTH);
		
		guiBoard=new Damier_GUI(game,variante);
		guiBoard.addMouseListener(mouse);
		getContentPane().add(guiBoard); 
		setFocusable(true);
		
		setVisible(true);
		
		
		
		
	}
	/**
	 * Update the label informing which player have to play.
	 */

	public void changeCurrentPlayer(boolean isBlack)
	{
		if(isBlack){
			j2.setBorder(BorderFactory.createLineBorder(Color.red));
			j1.setBorder(BorderFactory.createEmptyBorder());
		}
		else{
			j1.setBorder(BorderFactory.createLineBorder(Color.red));
			j2.setBorder(BorderFactory.createEmptyBorder());
		}

	}


	public void printPopUp(String message)
	{
		JOptionPane.showMessageDialog(this,message);
	}
	public void printPlayerTime(String message, boolean isBlack)
	{
		if(isBlack)
			j2.setText(message);
		else
			j1.setText(message);
	}

	public Damier_GUI getBoard() {
		return guiBoard;
	}



	public Game getGame() {
		return game;
	}
}

