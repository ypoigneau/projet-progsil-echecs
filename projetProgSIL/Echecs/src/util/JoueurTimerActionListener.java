package util;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import control.Game;

import model.Joueur;

public class JoueurTimerActionListener implements ActionListener {
    private int timeMinute;
    private int timeSeconde;
    private Joueur joueur;
    private Game game;
    

    public JoueurTimerActionListener(int initMinute, int initSeconde, Joueur j,Game g) {
          super();
          this.timeMinute = initMinute;
          this.timeSeconde = initSeconde;
          this.joueur = j;
          game=g;
    }

    public void actionPerformed(ActionEvent e) {
    	if (this.timeSeconde == 0 && this.timeMinute==0)
    		game.endGame(joueur);
    	
          if (this.timeSeconde == 0) {
                this.timeMinute--;
                this.timeSeconde = 59;
          } else
                this.timeSeconde--;
          
          joueur.updateTime(timeMinute, timeSeconde);
    }
    
    

}