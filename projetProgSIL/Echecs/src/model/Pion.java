package model;

public class Pion extends Piece implements I_Piece{

	public Pion(int x, int y, boolean isBlack) {
		super(x, y, isBlack);
	}

	@Override
	public boolean mouvementValide(int x, int y, Piece[] board) {
		int size=board.length/8;
		if(this.isBlack)
		{
			if(this.y+1==y && this.x==x)
			{
				if(board[x-1+(y-1)*size]!=null)	return false;
				return true;
			}
			if(this.y==2 && this.y+2==y && this.x==x)
			{
				if(board[x-1+(y-1)*size]!=null || board[x-1+(y-2)*size]!=null)	return false;
				return true;	
			}
			
			if(this.y+1==y && (this.x+1==x || this.x-1==x))
			{
				if(board[x-1+(y-1)*size]!=null)
				{
					if(board[x-1+(y-1)*size].isBlack)	return false;
					return true;
				}
				return false;
			}
			return false;
		}
		if(this.y-1==y && this.x==x)
		{
			if(board[x-1+(y-1)*size]!=null)	return false;
			return true;
		}
		if(this.y==7)
		{
			if(this.y-2==y && this.x==x)
			{
				if(board[x-1+(y-1)*size]!=null || board[x-1+y*size]!=null)	return false;
				return true;
			}
		}
		
		if(this.y-1==y && (this.x+1==x || this.x-1==x))
		{
			if(board[x-1+(y-1)*size]!=null)
			{
				if(!board[x-1+(y-1)*size].isBlack)	return false;
				return true;
			}
			return false;
		}
		
		return false;
	}

	public String getType() {
		return "Pion";
	}
}