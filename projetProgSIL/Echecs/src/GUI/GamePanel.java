package GUI;

import java.awt.BorderLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class GamePanel extends JPanel{
	
	private JLabel labelJoueur1;
	private JLabel labelJoueur2;
	private JLabel labelTour;
	
	public GamePanel() {
		
		this.setSize(50, 50);
		labelJoueur1=new JLabel("Joueur Blanc :");
		labelJoueur2=new JLabel("Joueur Noir :");
		
		labelTour=new JLabel("C'est au joueur blanc de jouer.");
		
		this.add(labelJoueur1, BorderLayout.NORTH); 
		this.add(labelJoueur2,BorderLayout.CENTER);
		this.add(labelTour,BorderLayout.SOUTH);
		
	}
	
	
	

}
