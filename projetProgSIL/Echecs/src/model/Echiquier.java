package model;

/**
 * Implements the board of the game.
 */
public class Echiquier {

	private Piece[] board;
	private int[] RoiNoir;
	private int[] RoiBlanc;
	private boolean isEchecPresentBlanc;
	private boolean isEchecPresentNoir;
	private boolean isMat;
	public Piece[] getBoard() 
	{
		return board.clone();
	}

	public Piece getPiece(int x, int y)
	{
		return board[ x - 1 + ( y - 1 ) * 8 ];
	}


	public Echiquier()
	{
		board = new Piece[64];
		RoiBlanc=new int[]{5,8};
		RoiNoir=new int[]{5,1};
		isEchecPresentBlanc = false;
		isEchecPresentNoir = false;
		isMat=false;


		initBoard();
	}

	void initBoard()
	{
		placer(new Tour(1,1,true));
		placer(new Cavalier(2,1,true));
		placer(new Fou(3,1,true));
		placer(new Reine(4,1,true));
		placer(new Roi(5,1,true));
		placer(new Fou(6,1,true));
		placer(new Cavalier(7,1,true));
		placer(new Tour(8,1,true)); 
		for (int i=1; i <=8; i++)
			placer(new Pion(i, 2, true));

		placer(new Tour(1,8,false));
		placer(new Cavalier(2,8,false));
		placer(new Fou(3,8,false));
		placer(new Reine(4,8,false));
		placer(new Roi(5,8,false));
		placer(new Fou(6,8,false));
		placer(new Cavalier(7,8,false));
		placer(new Tour(8,8,false)); 
		for (int i=1; i <=8; i++)
			placer(new Pion(i, 7, false));

	}



	/**
	 * Allow to put a piece on the board
	 * @param p the piece to add on the board	 
	 */
	boolean placer( Piece p )
	{
		if ( ( p.getX() >= 1 ) && ( p.getX() <= 8 )
				&& ( p.getY() >= 1 ) && (  p.getY()  <= 8 ) 
				&& ( p != null ))
		{
			board[  p.getX()  - 1 + ( p.getY()  - 1 ) * 8 ] = p;
			return true;
		}
		return false;
	}

	/**
	 * Allow to print the board in console.
	 */
	public void affiche()
	{
		for ( int y = 1; y <=8; y++ )
		{
			System.out.print(y);
			for ( int x = 1; x <= 8; ++x )
			{
				char c;
				Piece p = getPiece( x, y );
				if ( p == null ) 
					System.out.print("|   -  |");
				else
					System.out.print("| "+p.getType()+ " |");
			}
			System.out.println(" " + y );
		}


	}


	public boolean deplacer(Piece p, int x, int y)
	{
		if(p.mouvementValide(x,y,board))
		{
			Piece toDelete = board[x-1 + (y-1) * 8];
			boolean isPieceDeleted = false;
			boolean cancel = false;
			int origX = p.getX();
			int origY = p.getY();
			
			board[p.getX()-1 + (p.getY()-1) * 8]=null;

			p.setX(x);
			p.setY(y);

			if(p.getType()=="Roi")
			{
				if (p.isBlack!= true)
				{
					RoiBlanc[0] = p.getX(); 
					RoiBlanc[1] = p.getY(); 
				}
				else
				{
					RoiNoir[0] = p.getX();
					RoiNoir[1] = p.getY();
				}
			}

			if(board[x-1 + (y-1) * 8]!= null){
				
				deletePiece(board,x,y);
				isPieceDeleted = true;
			}
			board[x-1 + (y-1) * 8]=p;
			boolean echecwaspresentBlanc = isEchecPresentBlanc;
			boolean echecwaspresentNoir = isEchecPresentNoir;
			if(verifierEchec())
			{
				if(verifierMat(p))
				{
					isMat=true;
				}
				else
				{
					isMat=false;
				}

				if((echecwaspresentBlanc && !p.getIsBlack()) ||(echecwaspresentNoir && p.getIsBlack()) || (isEchecPresentBlanc && !p.getIsBlack()) ||(isEchecPresentNoir && p.getIsBlack()) )
					cancel = true;

			}
			else{
				
				echecwaspresentBlanc = false;
				echecwaspresentNoir = false;
			}

			if(cancel)
			{
				p.setX(origX);
				p.setY(origY);
				if(p.getType()=="Roi")
				{
					if (p.isBlack!= true)
					{
						RoiBlanc[0] = p.getX(); 
						RoiBlanc[1] = p.getY(); 
					}
					else
					{
						RoiNoir[0] = p.getX();
						RoiNoir[1] = p.getY();
					}
				}
				deletePiece(board,x,y);
				board[origX-1 + (origY-1) * 8]=p;
				if(isPieceDeleted)
				{
					board[x-1 + (y-1) * 8]=toDelete;
				}
				return false;
			}
			return true;

		}
		return false;
	}

	public void deletePiece(Piece[] board, int x,int y){
		board[x-1 + (y-1) * 8]=null;
	}
	public boolean verifierEchec()
	{
		for(int i=0; i< board.length;i++)
		{

			Piece p = board[i];
			if(p!=null){
				if (p.getType()!="Roi")
				{
					if(p.isBlack!= true)
					{
						if(p.mouvementValide(RoiNoir[0], RoiNoir[1], board)== true)
						{
							System.out.println("Echec! par le pion en"+p.getX()+":"+p.getY()+" sur roi noir ("+RoiNoir[0]+";"+ RoiNoir[1]+")");
							if(!isEchecPresentNoir)
							{
								
								isEchecPresentNoir = true;
							}
							return true;
						}
					}
					else
					{
						if(p.mouvementValide(RoiBlanc[0], RoiBlanc[1], board)== true)
						{
							System.out.println("Echec! par le pion en"+p.getX()+":"+p.getY()+" sur roi blanc ("+RoiBlanc[0]+";"+ RoiBlanc[1]+")");
							if(!isEchecPresentBlanc)
							{
								isEchecPresentBlanc = true;
							}
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	public boolean verifierMat(Piece pEchec){

		if (isEchecPresentBlanc == true){
			int xBlanc = RoiBlanc[0];
			int yBlanc = RoiBlanc[1];       
			for (int x = xBlanc-1; x <= xBlanc+1; x++){
				for (int y = yBlanc-1; y <= yBlanc+1; y++){
					if(getPiece(xBlanc, yBlanc).mouvementValide(x, y, board)){
						RoiBlanc[0]=x;
						RoiBlanc[1]=y;
						if(!verifierEchec())
						{
							RoiBlanc[0]=xBlanc;
							RoiBlanc[1]=yBlanc;
							return false;
						}
						else
						{
							RoiBlanc[0]=xBlanc;
							RoiBlanc[1]=yBlanc;
						}
					}
				}
			}
			for(int i=0; i< board.length;i++)
			{
				Piece p = board[i];
				if(p!=null){
					if (p.getType()!="Roi")
					{
						if(p.isBlack!= true)
						{
							if(p.mouvementValide(pEchec.getX(), pEchec.getY(), board)== true)
							{
								return false;
							}
						}
					}
				}
			}
		}
		else if (isEchecPresentNoir == true){
			int xNoir = RoiNoir[0];
			int yNoir = RoiNoir[1];
			for (int x = xNoir-1; x <= xNoir+1; x++){
				for (int y = yNoir-1; y <= yNoir+1; y++){
					if(getPiece(xNoir, yNoir).mouvementValide(x, y, board)){
						RoiNoir[0]=x;
						RoiNoir[1]=y;
						if(!verifierEchec())
						{
							
							RoiNoir[0]=xNoir;
							RoiNoir[1]=yNoir;
							return false;
						}
						else
						{
							RoiNoir[0]=xNoir;
							RoiNoir[1]=yNoir;
						}
					}
				}
			}
			for(int i=0; i< board.length;i++)
			{
				Piece p = board[i];
				if(p!=null){
					if (p.getType()!="Roi")
					{
						if(p.isBlack!= true)
						{
							if(p.mouvementValide(pEchec.getX(), pEchec.getY(), board)== true)
							{
								
								return false;
							}
						}
					}
				}
			}

		}
		return true;
	}

	public boolean isMat() {

		return isMat;
	}
}
