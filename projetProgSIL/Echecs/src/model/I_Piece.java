package model;

public interface I_Piece {
	public boolean mouvementValide(int x, int y, Piece[] board);
	public String getType();
}
