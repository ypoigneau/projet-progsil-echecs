package GUI;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import javax.swing.JPanel;
import control.Game;
import model.Piece;


/**
 * The gui board of the game
 */
public class Damier_GUI extends JPanel{
	
	private Game game;
	private int variante;

	public Game getGame() {
		return game;
	}

	public Damier_GUI(Game game,int variante){
		this.game=game;		
		this.variante=variante;
	}

	/**
	 * Draw the initial gui board
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		int size=8;
		if(variante==1)
		{
			size=8;
		
			Image img = Toolkit.getDefaultToolkit().getImage("pictures/classic.gif") ;		  
			g.drawImage(img,50,50,null) ;
		}
		
		
		if(variante==2)
		{
			size=10;
		
			Image img = Toolkit.getDefaultToolkit().getImage("pictures/capablanca.gif") ;		  
			g.drawImage(img,50,50,null) ;
		}
			for (int y = 1; y <=8; y++)
				for (int x = 1; x <=size; x++) {		         

					Piece p=game.getPiece(x, y);
					if(p!=null)
						putPiece(g,p.getType(),p.getIsBlack(),x,y);
				}
				  
	}
	/*
	 * Put a piece on the board.
	 */
	public void putPiece(Graphics g, String type, boolean color, int x, int y){
		Image img = Toolkit.getDefaultToolkit().getImage("pictures/"+type+"_"+(color==true?"noir":"blanc")+".gif");
		g.drawImage(img, x * 50, y * 50,null);   
	}
}