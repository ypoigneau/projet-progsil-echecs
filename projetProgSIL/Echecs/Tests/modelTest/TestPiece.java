package modelTest;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import model.Cavalier;
import model.Centaure;
import model.Echiquier;
import model.Echiquier_capablanca;
import model.Fou;
import model.Imperatrice;
import model.Piece;
import model.Pion;
import model.Reine;
import model.Roi;
import model.Tour;

import org.junit.Test;


public class TestPiece {

	@Test
	public void TestPieces(){
		Piece P1 = new Piece(2, 4, true);
		assertTrue(P1.getIsBlack());
		assertEquals(P1.getX(), 2);
		assertEquals(P1.getY(), 4);
	}


	@Test
	public void TestPieceRoiProprietes(){
		Piece P1 = new Roi(5, 1, true);
		String roi = "Roi";
		assertTrue(P1.getIsBlack());
		assertEquals(P1.getX(), 5);
		assertEquals(P1.getY(), 1);
		assertEquals(P1.getType(),roi);
	}

	@Test
	public void TestPieceRoiMouvements(){
		Piece P1 = new Roi(5, 1, true);
		Echiquier board= new Echiquier();
		assertFalse(P1.mouvementValide(4, 2,board.getBoard()));
		assertFalse(P1.mouvementValide(3, 2,board.getBoard()));
		assertFalse(P1.mouvementValide(3, 1,board.getBoard()));
		assertFalse(P1.mouvementValide(5, 1,board.getBoard()));
		assertFalse(P1.mouvementValide(5, 2,board.getBoard()));
		assertFalse(P1.mouvementValide(5, 6,board.getBoard()));

	}

	@Test
	public void TestPieceReineProprietes(){
		String reine = "Reine";
		Piece P1 = new Reine(3, 1, true);
		assertTrue(P1.getIsBlack());
		assertEquals(P1.getX(), 3);
		assertEquals(P1.getY(), 1);
		assertEquals(P1.getType(),reine);
	}

	@Test
	public void TestPieceReineMouvements(){
		Echiquier board= new Echiquier();
		Piece P1 = new Reine(3,1,true);
		assertFalse(P1.mouvementValide(3, 2,board.getBoard()));
	}

	@Test
	public void TestPieceCavalierProprietes(){
		String cavalier = "Cavalier";
		Piece P1 = new Cavalier(2, 0, true);
		assertTrue(P1.getIsBlack());
		assertEquals(P1.getX(), 2);
		assertEquals(P1.getY(), 0);
		assertEquals(P1.getType(),cavalier);
	}

	@Test
	public void TestPieceCavalierMouvements(){
		Echiquier board= new Echiquier();
		Piece P1 = new Cavalier(2, 0, true);
		assertFalse(P1.mouvementValide(3, 2,board.getBoard()));
		assertFalse(P1.mouvementValide(4, 1,board.getBoard()));
	}

	@Test
	public void TestPieceFouProprietes(){
		String fou = "Fou";
		Piece P1 = new Fou(2, 0, true);
		assertTrue(P1.getIsBlack());
		assertEquals(P1.getX(), 2);
		assertEquals(P1.getY(), 0);
		assertEquals(P1.getType(),fou);
	}

	@Test
	public void TestPieceFouMouvements(){
		Echiquier board= new Echiquier();
		Piece P1 = new Fou(2, 0, true);
		assertFalse(P1.mouvementValide(3, 1,board.getBoard()));
		assertFalse(P1.mouvementValide(4, 2,board.getBoard()));
	}

	@Test
	public void TestPieceTourProprietes(){
		String tour = "Tour";
		Piece P1 = new Tour(0, 0, true);
		assertTrue(P1.getIsBlack());
		assertEquals(P1.getX(), 0);
		assertEquals(P1.getY(), 0);
		assertEquals(P1.getType(), tour);
	}

	@Test
	public void TestPieceTourMouvements(){
		Echiquier board= new Echiquier();
		Piece P1 = new Tour(1, 1, true);
		assertFalse(P1.mouvementValide(1, 7,board.getBoard()));
		assertFalse(P1.mouvementValide(1, 2,board.getBoard()));
	}

	@Test
	public void TestPiecePionProprietes(){
		String pion = "Pion";
		Piece P1 = new Pion(2, 2, true);
		assertTrue(P1.getIsBlack());
		assertEquals(P1.getX(), 2);
		assertEquals(P1.getY(), 2);
		assertEquals(P1.getType(),pion);
	}

	@Test
	public void TestPiecePionMouvements(){
		Echiquier board= new Echiquier();
		Piece P1 = new Pion(2, 2, true);
		assertTrue(P1.mouvementValide(2, 4,board.getBoard()));
		assertTrue(P1.mouvementValide(2, 3,board.getBoard()));
		assertFalse(P1.mouvementValide(2, 5,board.getBoard()));
	}

	@Test
	public void TestPieceImperatriceProprietes(){
		String imperatrice = "Imperatrice";
		Imperatrice P1 = new Imperatrice(8,1,true);
		assertTrue(P1.getIsBlack());
		assertEquals(P1.getX(), 8);
		assertEquals(P1.getY(), 1);
		assertEquals(P1.getType(),imperatrice);
	}

	@Test
	public void TestPieceImperatriceMouvements(){
		Echiquier_capablanca board= new Echiquier_capablanca();
		Imperatrice P1 = new Imperatrice(8,1,true);
		assertTrue(P1.mouvementValide(9, 3,board.getBoard()));
		assertTrue(P1.mouvementValide(7, 3,board.getBoard()));
		assertFalse(P1.mouvementValide(6, 3,board.getBoard()));
	}

	@Test
	public void TestPieceCentaureProprietes(){
		String centaure = "Centaure";
		Centaure P1 = new Centaure(3,1,true);
		assertTrue(P1.getIsBlack());
		assertEquals(P1.getX(), 3);
		assertEquals(P1.getY(), 1);
		assertEquals(P1.getType(),centaure);
	}

	@Test
	public void TestPieceCentaureMouvements(){
		Echiquier_capablanca board= new Echiquier_capablanca();
		Centaure P1 = new Centaure(3,1,true);
		assertTrue(P1.mouvementValide(4, 3,board.getBoard()));
		assertTrue(P1.mouvementValide(2, 3,board.getBoard()));
		assertFalse(P1.mouvementValide(3, 6,board.getBoard()));
	}


	@Test
	public void TestPiecePionManger(){
		Echiquier board= new Echiquier();
		Piece P1 = new Pion(2, 2, true);
		Piece P2 = new Pion(3, 7, false);
		assertTrue(board.deplacer(P1, 2, 4));
		assertTrue(board.deplacer(P2, 3, 5));
		assertTrue(board.deplacer(P1, 3, 5));
		assertEquals(board.getPiece(3, 5),P1);
	}

	@Test
	public void TestEchec(){
		Echiquier board= new Echiquier();
		Piece PN1 = new Pion(8, 2, true);
		Piece TN1 = new Tour(8,1,true);
		Piece PB1 = new Pion(5, 7, false);
		Piece RB1 = new Roi(5,8,false);
		assertTrue(board.deplacer(PN1, 8, 4));
		assertTrue(board.deplacer(TN1, 8, 3));
		assertTrue(board.deplacer(TN1, 5, 3));
		assertTrue(board.deplacer(TN1, 5, 7));
		assertTrue(board.deplacer(TN1, 5, 8));

	}
}
