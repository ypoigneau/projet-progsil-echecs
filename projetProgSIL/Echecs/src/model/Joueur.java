package model;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;

import control.Game;

import util.Constantes;
import util.JoueurTimerActionListener;

public class Joueur {
	private int m_score;
	private boolean m_isBlack;
	private String m_name;
	private Timer time;
	private Game game;
	private JoueurTimerActionListener actionTimer;

	public Joueur(String name, boolean isBlack, Game g)
	{
		
		game = g;
		m_score = 0;
		m_name = name;
		m_isBlack = isBlack;
		actionTimer=new JoueurTimerActionListener((Constantes.reflexionTime/1000)/60,(Constantes.reflexionTime/1000)%60, this,game);
		time = new Timer(1000, actionTimer );	
		updateTime(30, 00);
		
		
	}
	
	
	public void restartTime()
	{
		time.removeActionListener(actionTimer);
		actionTimer=new JoueurTimerActionListener((Constantes.reflexionTime/1000)/60,(Constantes.reflexionTime/1000)%60, this,game);
		time=new Timer(1000, actionTimer);	
		time.restart();
		time.stop();
	}
	
	
		
	public void updateTime(int minutes, int secondes)
	{
		String sec = (new Integer(secondes)).toString();
		if(secondes<10){
			sec = "0"+sec;
		}
		game.updateTime(this.m_name+": "+minutes+":"+sec,m_isBlack);
	}
	
	public void startTime()
	{
		
		this.time.start();
	}
	
	public void stopTime()
	{
		
		this.time.stop();
	}

	public int getScore() {
		return m_score;
	}

	public void setScore(int mScore) {
		m_score = mScore;
	}

	public boolean isBlack() {
		return m_isBlack;
	}

	public void setIsBlack(boolean mIsBlack) {
		m_isBlack = mIsBlack;
	}

	public String getM_name() {
		return m_name;
	}

	public void setM_name(String mName) {
		m_name = mName;
	}



	
}
