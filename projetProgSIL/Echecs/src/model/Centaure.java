package model;

public class Centaure extends Piece implements I_Piece{

	public Centaure(int x, int y, boolean isBlack) {
		super(x, y, isBlack);
	}
	
	public String getType() {
		return "Centaure";
	}
	
	public boolean mouvementValide(int x, int y, Piece[] board) {
		int size=board.length/8;
		int depX=this.x-x;
        int depY=this.y-y;
        int i,j;
        boolean xSupZero=true;
        boolean ySupZero=true;
        
        //Verifie si meme couleur
        if(board[x-1+(y-1)*size]!=null)
        {
        	if(board[x-1+(y-1)*size].getIsBlack()==this.isBlack)	return false;
        }
		if(((this.x+2==x || this.x-2==x) && (this.y+1==y || this.y-1==y)) || ((this.y+2==y || this.y-2==y) && (this.x+1==x || this.x-1==x)))
		{
			if(board[x-1+(y-1)*size]!=null)
			{
				if(board[x-1+(y-1)*size].getIsBlack()==this.isBlack)	return false;
			}
			return true;
		}
        
        if(depX<0)	xSupZero=false;
        if(depY<0)	ySupZero=false;
        
        //on verifie qu'il n'y a pas de piece dans la trajectoire
        if(xSupZero)
        {
        	if(ySupZero)
    		{
        		j= this.y-1;
	        	for(i=this.x-1;i>x;i--)
	        	{
	        		if(board[i-1+(j-1)*size]!=null)
	        		{
	        			
	        			return false;
	        		}
	        		j--;
	        	}
    		}
        	else
        	{
        		j=this.y+1;
        		for(i=this.x-1;i>x;i--)
	        	{
		        	if(board[i-1+(j-1)*size]!=null)
		        	{
		       			
		       			return false;
		        	}
		        	j++;
	        	}
        	}
        }
        else
        {
        	if(ySupZero)
    		{
        		j=this.y-1;
        		for(i=this.x+1;i<x;i++)
        		{
        			if(board[i-1+(j-1)*size]!=null)
		        	{
		        		
		        		return false;
		       		}
		        	j--;
        		}
    		}
        	else
        	{
        		j=this.y+1;
        		for(i=this.x+1;i<x;i++)
        		{		        	
		        	if(board[i-1+(j-1)*size]!=null)
		        	{
		        		
		        		return false;
		        	}
		        	j++;
        		}		
        	}
        }

        if(depX==depY)
            return true; 
        if(depX<0)
            return (-depX==depY);
        if(depY<0)
            return (depX==-depY);
		return false;
	}
}
