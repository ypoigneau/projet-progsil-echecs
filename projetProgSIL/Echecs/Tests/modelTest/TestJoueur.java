package modelTest;

import static org.junit.Assert.*;

import javax.swing.Timer;

import org.junit.Test;

import util.Constantes;
import util.JoueurTimerActionListener;

import control.Game;

import model.Echiquier;
import model.Joueur;

public class TestJoueur {
	@Test
	public void testCreation()
	{
		Echiquier e = new Echiquier();
		Game g = new Game(e);
		Joueur j1 = new Joueur("nom",true,g);
		assertEquals(j1.getM_name(), "nom");
		assertTrue(j1.isBlack());
		assertEquals(j1.getScore(),0);
	}
	
	@Test
	public void testScore()
	{
		Echiquier e = new Echiquier();
		Game g = new Game(e);
		Joueur j1 = new Joueur("nom",true,g);
		j1.setScore(8);
		assertEquals(j1.getScore(),8);
	}
	
}
