package control;
import GUI.Game_GUI;
import model.Echiquier;
import model.Echiquier_capablanca;
import model.Joueur;
import model.Piece;



/**This object launch a new game.
 */
public class Game {

	private boolean currentPlayer;
	
	private Echiquier board;
	private Game_GUI gui;
	private Joueur joueur1;
	private Joueur joueur2;

	public Game(Echiquier b){
		board=b;
		currentPlayer = false;
		gui=new Game_GUI(this,1);
		joueur1 = new Joueur("Joueur blanc",false, this);
		joueur2 = new Joueur("Joueur noir",true, this); 


		joueur1.startTime();
	}
	public boolean getCurrentPlayer(){
		return currentPlayer;
	}
	
	public Piece getPiece(int x,int y)
	{
		return board.getPiece(x, y);
	}
	
	public void endGame(Joueur j)
	{
		gui.printPopUp("Fin de partie: "+j.getM_name() +" a perdu.");
		System.exit(0);
	}

	public void changeCurrentPlayer(){

		currentPlayer = !currentPlayer;
		gui.changeCurrentPlayer(currentPlayer);
		if(currentPlayer)
		{
			joueur2.startTime();
			joueur1.stopTime();
		}
		else{
			joueur1.startTime();
			joueur2.stopTime();
		}
	}	

	
	public void isEchec() {
		gui.printPopUp("Echecs");
	}
	public void changeBoard(int variante)
	{
		
		gui.dispose();		
		if(variante==1)
			board=new Echiquier();
		if(variante==2)
			board=new Echiquier_capablanca();
		joueur1.restartTime();
		joueur2.restartTime();
		gui=new Game_GUI(this, variante);
		gui.repaint();
		joueur1.startTime();

	}

	public Game_GUI getGui() {
		return gui;
	}

	public void updateTime(String message,boolean isBlack)
	{
		gui.printPlayerTime(message, isBlack);
	}

	public void move(Piece p,int x,int y)
	{
		
		boolean verif=board.deplacer(p, x, y);		
		
		//board.affiche();
		gui.repaint();
		if(verif == true)
		{
			changeCurrentPlayer();
			boolean echec=board.verifierEchec();
			boolean mat=board.verifierMat(p);
			if(echec)
				isEchec();
			if(board.isMat())
			{
				isMat();
				System.exit(0);
			}
				
				
		}
		else
		{
			gui.printPopUp("Coup invalide. La piece "+ p.getType()+"("+p.getX()+","+p.getY()+") ne peut se rendre en position ("+x+","+y+").");
		}
		gui.repaint();
	}
	private void isMat() {
		gui.printPopUp("Mat!");
		
	}
	public static void main(String[] args) {
		Game play=new Game(new Echiquier());
	}


}
