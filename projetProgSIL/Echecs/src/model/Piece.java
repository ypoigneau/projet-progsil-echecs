package model;
public class Piece implements I_Piece
{
	protected int x;
	protected int y;
	protected boolean isBlack;
	
	public Piece() {
		super();
	}
	public Piece(int x, int y, boolean isBlack) {
		super();
		this.x = x;
		this.y = y;
		this.isBlack = isBlack;
	}
	public boolean getIsBlack() {
		return isBlack;
	}
	public void setBlack(boolean isBlack) {
		this.isBlack = isBlack;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public boolean mouvementValide(int x, int y, Piece[] board){
		return true;
	}
	public String getType(){
		return "Piece";
	}	
}
