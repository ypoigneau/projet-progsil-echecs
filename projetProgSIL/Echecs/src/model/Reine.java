package model;

public class Reine extends Piece implements I_Piece {

	public Reine(int x, int y, boolean isBlack) {
		super(x, y, isBlack);
	}

	@Override
	public boolean mouvementValide(int x, int y, Piece[] board) {
		int size=board.length/8;
		int depX=this.x-x;
        int depY=this.y-y;
        int i,j;
        boolean xSupZero=true;
        boolean ySupZero=true;
        
      //Verifie si meme couleur
        if(board[x-1+(y-1)*size]!=null)
        {
        	if(board[x-1+(y-1)*size].getIsBlack()==this.isBlack)	return false;
        }
        
        if(depX<0)	xSupZero=false;
        if(depY<0)	ySupZero=false;
        
        if(this.y==y && this.x!=x)
		{
			depX=this.x-x;
			if(depX>0)
			{
				for(i=this.x-1;i>x;i--)
				{
					if(board[i-1+(y-1)*size]!=null)
						return false;
				}
			}
			for(i=this.x+1;i<x;i++)
			{
				if(board[i-1+(y-1)*size]!=null)
					return false;
			}
			return true;
		}
		
		if(this.x==x && this.y!=y)
		{
			depY=this.y-y;
			if(depY>0)
			{
				for(j=this.y-1;j>y;j--)
				{
					if(board[x-1+(j-1)*size]!=null)
						return false;
				}
			}
			for(j=this.y+1;j<y;j++)
			{
				if(board[x-1+(j-1)*size]!=null)
					return false;
			}
			return true;
		}
		
		if(xSupZero)
        {
        	if(ySupZero)
    		{
        		j= this.y-1;
	        	for(i=this.x-1;i>x;i--)
	        	{
	        		if(board[i-1+(j-1)*size]!=null)
	        		{
	        			
	        			return false;
	        		}
	        		j--;
	        	}
    		}
        	else
        	{
        		j=this.y+1;
        		for(i=this.x-1;i>x;i--)
	        	{
		        	if(board[i-1+(j-1)*size]!=null)
		        	{
		       			
		       			return false;
		        	}
		        	j++;
	        	}
        	}
        }
        else
        {
        	if(ySupZero)
    		{
        		j=this.y-1;
        		for(i=this.x+1;i<x;i++)
        		{
        			if(board[i-1+(j-1)*size]!=null)
		        	{
		        		
		        		return false;
		       		}
		        	j--;
        		}
    		}
        	else
        	{
        		j=this.y+1;
        		for(i=this.x+1;i<x;i++)
        		{		        	
		        	if(board[i-1+(j-1)*size]!=null)
		        	{
		        		return false;
		        	}
		        	j++;
        		}		
        	}
        }

        if(depX==depY)
            return true; 
        if(depX<0)
            return (-depX==depY);
        if(depY<0)
            return (depX==-depY);
		return false;
	}

	public String getType() {
		return "Reine";
	}
}
