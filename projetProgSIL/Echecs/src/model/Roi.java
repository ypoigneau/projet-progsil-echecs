package model;

public class Roi extends Piece implements I_Piece{

	public Roi(int x, int y, boolean isBlack) {
		super(x, y, isBlack);
	}

	@Override
	public boolean mouvementValide(int x, int y, Piece[] board) {
		int size=board.length/8;
		if(x < 1 || x > size || y < 1 || y > 8){
			return false;
		}
		if(this.x+1>=x && this.y+1>=y && this.x-1<=x && this.y-1<=y && !(this.x==x && this.y==y))	
		{
			if(board[x-1+(y-1)*size]!=null)
			{
				if(board[x-1+(y-1)*size].getIsBlack()==this.isBlack)	return false;
			}
			return true;
		}
		return false;
	}

	public String getType() {
		return "Roi";
	}
}
