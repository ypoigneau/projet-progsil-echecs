package model;

public class Echiquier_capablanca extends Echiquier {
	private Piece[] board;
	private int[] RoiNoir;
	private int[] RoiBlanc;
	private boolean isEchecPresentBlanc;
	private boolean isEchecPresentNoir;

	public Echiquier_capablanca() {

		initBoard();
	}
	@Override
	public Piece getPiece(int x, int y)
	{
		return board[ x - 1 + ( y - 1 ) * 10 ];
	}
	@Override
	void initBoard()
	{
		board = new Piece[80];
		RoiBlanc=new int[]{6,8};
		RoiNoir=new int[]{6,1};
		isEchecPresentBlanc = false;
		isEchecPresentNoir = false;
		placer(new Tour(1,1,true));
		placer(new Cavalier(2,1,true));
		placer(new Centaure(3,1,true));
		placer(new Fou(4,1,true));
		placer(new Reine(5,1,true));
		placer(new Roi(6,1,true));
		placer(new Fou(7,1,true));
		placer(new Imperatrice(8,1,true));
		placer(new Cavalier(9,1,true));
		placer(new Tour(10,1,true)); 
		for (int i=1; i <=10; i++)
			placer(new Pion(i, 2, true));

		placer(new Tour(1,8,false));
		placer(new Cavalier(2,8,false));
		placer(new Centaure(3,8,false));
		placer(new Fou(4,8,false));
		placer(new Reine(5,8,false));
		placer(new Roi(6,8,false));
		placer(new Fou(7,8,false));
		placer(new Imperatrice(8,8,false));
		placer(new Cavalier(9,8,false));
		placer(new Tour(10,8,false)); 
		for (int i=1; i <=10; i++)
			placer(new Pion(i, 7, false));


	}
	/**
	 * Allow to put a piece on the board
	 * @param p the piece to add on the board	 
	 */
	@Override
	boolean placer( Piece p )
	{
		if ( ( p.getX() >= 1 ) && ( p.getX() <= 10 )
				&& ( p.getY() >= 1 ) && (  p.getY()  <= 8 ) 
				&& ( p != null ))
		{
			
			board[  p.getX()  - 1 + ( p.getY()  - 1 ) * 10 ] = p;
			return true;
		}
		return false;
	}

	/**
	 * Allow to print the board in console.
	 */
	@Override
	public void affiche()
	{
		for ( int y = 1; y <=8; y++ )
		{
			System.out.print(y);
			for ( int x = 1; x <= 10; ++x )
			{
				char c;
				Piece p = getPiece( x, y );
				if ( p == null ) 
					System.out.print("|   -  |");
				else
					System.out.print("| "+p.getType()+ " |");
			}
			System.out.println(" " + y );
		}


	}
	@Override
	public boolean deplacer(Piece p, int x, int y)
	{
		if(p.mouvementValide(x,y,board))
		{
			Piece toDelete = board[x-1 + (y-1) * 10];
			boolean isPieceDeleted = false;
			boolean cancel = false;
			int origX = p.getX();
			int origY = p.getY();

			board[p.getX()-1 + (p.getY()-1) * 10]=null;

			p.setX(x);
			p.setY(y);

			if(board[x-1 + (y-1) * 10]!= null){

				deletePiece(board,x,y);
				isPieceDeleted = true;
			}
			board[x-1 + (y-1) * 10]=p;
			boolean echecwaspresentBlanc = isEchecPresentBlanc;
			boolean echecwaspresentNoir = isEchecPresentNoir;
			if(verifierEchec())
			{
				if((echecwaspresentBlanc && !p.getIsBlack()) ||(echecwaspresentNoir && p.getIsBlack()) || (isEchecPresentBlanc && !p.getIsBlack()) ||(isEchecPresentNoir && p.getIsBlack()) )
					cancel = true;

			}
			else{

				echecwaspresentBlanc = false;
				echecwaspresentNoir = false;
			}

			if(cancel)
			{
				p.setX(origX);
				p.setY(origY);
				deletePiece(board,x,y);
				board[origX-1 + (origY-1) * 10]=p;
				if(isPieceDeleted)
				{
					board[x-1 + (y-1) * 10]=toDelete;
				}
				return false;
			}
			if(p.getType()=="Roi")
			{
				if (p.isBlack!= true)
				{
					RoiBlanc[0] = p.getX(); 
					RoiBlanc[1] = p.getY(); 
				}
				else
				{
					RoiNoir[0] = p.getX();
					RoiNoir[1] = p.getY();
				}
			}
			return true;

		}
		return false;
	}
	@Override
	public void deletePiece(Piece[] board, int x,int y){
		board[x-1 + (y-1) * 10]=null;
	}
	@Override
	public boolean verifierEchec()
	{
		for(int i=0; i< board.length;i++)
		{

			Piece p = board[i];
			if(p!=null){
				if (p.getType()!="Roi")
				{
					if(p.isBlack!= true)
					{
						if(p.mouvementValide(RoiNoir[0], RoiNoir[1], board)== true)
						{
							System.out.println("Echec! par le pion en"+p.getX()+":"+p.getY()+" sur roi noir ("+RoiNoir[0]+";"+ RoiNoir[1]+")");
							if(!isEchecPresentNoir)
							{
								
								isEchecPresentNoir = true;
							}
							return true;
						}
					}
					else
					{
						if(p.mouvementValide(RoiBlanc[0], RoiBlanc[1], board)== true)
						{
							System.out.println("Echec! par le pion en"+p.getX()+":"+p.getY()+" sur roi blanc ("+RoiBlanc[0]+";"+ RoiBlanc[1]+")");
							if(!isEchecPresentBlanc)
							{
								
								isEchecPresentBlanc = true;
							}
							return true;
						}
					}
				}
			}
		}
		return false;
	}

}
